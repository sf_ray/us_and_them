from threading import Lock
import requests
import json
import time


class AT2API(object):
    """
    Class to handle all the AT2 API interactions
    """
    def __init__(self, hostname='autotest2.solidfire.net', username="automation@solidfire.com",
                 passwd="solidfire", debug=False, endpoint="1.0"):
        self.hostname = hostname
        self.debug = debug
        self.endpoint = endpoint

        # insert https unless it's already there
        if hostname.startswith('http'):
            self.__server_url = hostname
        else:
            self.__server_url = 'https://{}'.format(self.hostname)
        # insert path to API unless it's already there
        if 'json-rpc' in hostname:
            self.__api_url = hostname
        else:
            self.__api_url = '{}/json-rpc/{}'.format(self.__server_url, endpoint)
        self.r = requests.session()
        self.authenticate(username, passwd)

    def authenticate(self, username="automation@solidfire.com", passwd="solidfire"):
        """ Authenticate against AT2 so that we can use api methods.
            Adapted from ApiCommonUtils.py
        """
        if self.debug:
            print ('Authenticating, url: {}, user: {}, pass: {}'
                       .format(self.__server_url, username, passwd))
        self.r.auth = (username, passwd)
        with Lock():
            r = self.r.get(self.__server_url, verify=False)
            if not r.status_code == 200:
                raise RuntimeError("HTTPError: \n\t{}".format(r))
        return

    def call(self, method=None, params=None, return_all=False, retry=5, endpoint=None):
        """ Call an Autotest2 API method
        """
        if params is None:
            params = {}

        request = {
            'method': method,
            'params': params
        }

        # Make sure we set the proper endpoint
        url = self.__api_url
        if endpoint:
            url = "{}/json-rpc/{}/".format(self.__server_url, endpoint)

        if self.debug:
            print ("at2 request: {}".format(request))

        with Lock():
            inputdata = json.dumps(request)
            r = None
            # All 2.0 endpoint calls need to be a post, regardless
            if endpoint == "2.0":
                print ("Invoking with 2.0 endpoint, this MUST be a HTTP POST request per spec.")
                r = self.r.post(url, data=inputdata, verify=False)
            else:
                r = self.r.get(url, data=inputdata, verify=False)

            if r and not r.status_code == 200:
                raise RuntimeError("HTTPError: \n\t{}".format(r))

        if r:
            try:
                response = r.json()

                if return_all:
                    return response
                else:
                    if 'result' in response.keys():
                        return response.get('result')
                    else:
                        return response

            except AttributeError as e:
                print ('Is there a HTTPError or URLError? {}'.format(e))
            except ValueError as e:
                print ('Possible JSON error. {}'.format(e))

        if retry < 1:
            raise e
        else:
            time.sleep(5)
            return self.call(method=method, params=params, return_all=return_all, retry=retry-1)

    def get_task_instance_step_output(self, tisid):
        result = self.call(method='GetTaskInstanceStepStdout', params={"taskInstanceStepID": tisid})

        print result

if __name__ == "__main__":
    SAMPLE_TASK_INSTANCE_ID = 198245865
    at2api = AT2API()
    at2api.get_task_instance_step_output(SAMPLE_TASK_INSTANCE_ID)