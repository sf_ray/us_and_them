import csv
import datetime
import json
import pprint

import os
import requests
import random
import socket
import time

from at2_sdk.at2 import LegacyAt2Api
from collections import Counter

# REGRESSION CHUNKS
CURR_DIR = os.path.dirname(__file__)
DATA_DIR = os.path.join(CURR_DIR, "data")

CHUNKA_ID = 2612
CHUNKB_ID = 2613
CHUNKC_ID = 2616

REGRESSION_CHUNKS_EPIC_IDS = [CHUNKA_ID, CHUNKB_ID, CHUNKC_ID]
SAMPLE_TASK_INSTANCE_ID = 5004493

LACP_SUTIES = {4995893: [4996448,
                         4996921,
                         4997365,
                         4998254,
                         4998742,
                         4999249,
                         4999812,
                         4999994,
                         5000253,
                         5000427,
                         5000642]}

CL_FULL_SUITES = [4995902, 4992733, 4992293, 4991592, 4991557, 4999109, 5000943, 5004505, 5007545,
                  5007540]
LACP_STEP_DISPLAY_NAME = "Enable bond mode on all nodes with interface provided"
CREATE_CLUSTER_WRAPPER = "Create Cluster Wrapper"
FILL_TO_STAGE_N_TASKS = ["Fill volumes", "[Create and Fill Volumes] Fill to Stage N",
                         "[Generic] Prefill Cluster to Stage"]
ENABLE_ENCRYPTION_AT_REST = ["enable_encryption_at_rest"]

CFULL_TAGS_STAGES = {"Cluster Full": ["stage2", "stage3", "stage4", "stage5"],
                     "RemoteRep": ["stage2"],
                     "ARF-Nodes": ["stage3"]}
CLUSTER_CONFIG_FIELDS = ["suite_name", "node_count", "epic_id", "task_id", "task_instance_id",
                         "suite_run_id", "testcases", "SF4805", 'SF9605', 'SF6010', 'SF2405',
                         'SF9010', 'SF3010']

SUITE_INFO_MANUAL_FIELD_NAMES = ["suite_name", "node_count", "suite_id",
                                "testcases", "SF4805", 'SF9605', 'SF6010', 'SF2405',
                                'SF9010', 'SF3010', "SF19210"]
DEFAULT_BOND_MODE = "ActivePassive"


# EAR INFO
AUTO_DISABLED_EAR_SUITES = {
 "API Cluster Suite": 5005495,
 "API Limits": 4991610,
 "[RemoteRep] 8-Node Remote Rep Suite": 4995902,
 "[RemoteRep] Snapshots in Replication Test Suite_ChunkA": 5000943,
 "[TestSuite]10-Node-ZooKeeper Supplemental": 5005801,
 "[TestSuite]5-Node-ZooKeeper Regression Suite": 5007552,
 "[TestSuite]5-Node-ZooKeeper Supplemental": 5004438,
 "[TestSuite]Customer Installable - Node At all States": 4994486,
 "[VAAI] VAAI Test Suite": 4997437,
 "[cdp-suite] - CDP Ungraceful - Stage X - Suite A - RFMS": 4996599,
 "[cdp-suite] - Graceful Removals - Stage X - Suite": 4992399,
 "[cdp-suite] - Sunny Day - Stage 2 - Suite": 4991588,
 "[iSCSI] Connectivity Regression": 4992425,
 "[nodefail] Node Failure Regression Suite": 4995882,
 "[volprov] Volume Provisioning Regression": 4991569
}


def post_request(data):
    """ Send HTTP Request
    :param dict data: Data to send as part of Request
    :return:
    """
    host_url = 'http://komodo.eng.solidfire.net/json-rpc/1.0'
    response = requests.post(host_url, json=data)
    return response.json()


def _call_method(method, params=None):
    """ Call Http Request with a given method.
    :param str host: Host IP
    :param str method: Method Name
    :param dict params: Parameters to be passed
    :return: response in dictionary format
    """

    host_url = 'http://komodo.eng.solidfire.net/json-rpc/1.0'
    success = False
    count = 0
    retry_interval = 5
    if params is None:
        params = {}
    while not success and count < 5:
        try:
            jsonrpc_id = int(random.random() * 100)
            data = {'params': params, "method": method, 'id': jsonrpc_id}
            response = post_request(data)
            if "error" in response:
                raise Exception(response["error"])
            return response["result"]
        except requests.exceptions.ConnectionError as rsp_error:
            print ("Received a ConnectionError during "
                       "'{}': {}".format(str(data), rsp_error))

        except requests.exceptions.HTTPError as rsp_error:
            print ("Received a HTTPError during "
                       "'{}': {}".format(str(data), rsp_error))

        except requests.exceptions.RequestException as rsp_error:
            print ("Received a RequestException during "
                       "'{}': {}".format(str(data), rsp_error))

        except socket.error as rsp_error:
            now = datetime.datetime.now()
            msg = "Received a socket.error that hasn't been defined by "
            msg += "requests or AT. You should investigate this as it might "
            msg += "be an environmental issue. The error occured during "
            msg += "'{}': {} at: {}".format(str(data), rsp_error, now)
            print (msg)

        count += 1
        print ("Retrying in {} seconds.".format(retry_interval))
        time.sleep(retry_interval)

    raise Exception("There were problems communicating with "
                    "komodo: {}.".format(host_url))


def get_suites_for_interval(days):
    suites = _call_method("get_suites_for_interval", {"days": days})
    return suites

def get_suites_for_epic_run(epic_run_id):
    suites = _call_method("get_suites_for_epic_run", params={"epic_run_id": epic_run_id})
    # pprint.pprint(suites)
    return suites


def write_data_to_csv(data, filename, fieldnames):
    with open(filename, 'w+') as fp:
        writer = csv.DictWriter(fp, fieldnames=fieldnames)
        writer.writeheader()
        writer.writerows(data)


def get_step_by_name(task_instance_id, step_name):
    at2_api = LegacyAt2Api(username="automation@solidfire.com", password="solidfire")
    resources = at2_api.get_step_by_name(task_instance_id, step_name)
    return resources


def get_platform_info_for_task_instance(task_instance_id):
    platforms = {}
    resources = get_step_by_name(task_instance_id, "reserve_nodes")
    for resource in resources:
        nodes = [node["model"] for node in resource["outputValue"]]
        platforms.update(Counter(nodes).items())
    # pprint.pprint(platforms)
    return platforms


def get_suites_by_epic(epics):
    suites = []
    for chunk_id in REGRESSION_CHUNKS_EPIC_IDS:
        suites.extend(get_suites_for_epic_run(chunk_id))
    return suites


def get_lacp_suites(suites):
    lacp_suites = []
    at2_api = LegacyAt2Api(username="automation@solidfire.com", password="solidfire")
    for suite in suites:
        steps = at2_api.get_step_by_name(suite, "clean_switch_ports")
        if steps:
            lacp_suites.append(suite)
    return lacp_suites


def get_test_cases_from_task_instance(task_instance_id):
    at2_api = LegacyAt2Api(username="automation@solidfire.com", password="solidfire")
    testcases = at2_api.list_all_tasks_with_tag(task_instance_id, tags=["TestCase"])
    return testcases


def get_lacp_tasks(lacp_suites):
    at2_api = LegacyAt2Api(username="automation@solidfire.com", password="solidfire")
    lacp_tasks = []
    for suite in lacp_suites:
        lacp_test_cases = at2_api.list_all_tasks_with_tag(suite, tags=["TestCase"])
        lacp_tasks.extend([task["taskAsStepTaskInstanceID"] for task in lacp_test_cases])
    return lacp_tasks


def get_suite_info():
    data = []
    suites = get_suites_by_epic(REGRESSION_CHUNKS_EPIC_IDS)
    for suite in suites:
        dt = {"suite_name": suite["suite_name"],
              "node_count": suite["nodes"],
              "epic_id": suite["epic_id"],
              "task_id": suite["task_id"],
              "task_instance_id": suite["task_instance_id"],
              "suite_run_id": suite["suite_run_id"]
             }
        platforms = get_platform_info_for_task_instance(dt["task_instance_id"])
        dt["testcases"] = suite["total_testcases"]
        dt.update(platforms)
        data.append(dt)
    return data


def write_suite_info_manual():
    with open(os.path.join(DATA_DIR, "manual_reg.json")) as fp:
        suites = json.load(fp)
    data = [{k: suite.get(k, 0) for k in SUITE_INFO_MANUAL_FIELD_NAMES} for suite in suites]

    file_name = os.path.join(DATA_DIR, "manual_suite_info.csv")
    write_data_to_csv(data, file_name, SUITE_INFO_MANUAL_FIELD_NAMES)


def write_suite_info_to_csv():
    data = get_suite_info()
    file_name = "{}/suite_info.csv".format(DATA_DIR)
    write_data_to_csv(data, file_name, CLUSTER_CONFIG_FIELDS)


def get_step_input_values(step, step_input_names):
    input_names = []
    for step_input in step["inputs"]:
        if step_input["inputName"] in step_input_names:
            raw_value = step_input["rawValue"]
            if raw_value:
                input_names.append(json.loads(raw_value)['value'])
            else:
                input_names.append(json.loads(step_input["rawInput"]))
    return input_names


def get_steps_by_display_names(tid, dnames):
    tsteps = []
    at2_api = LegacyAt2Api(username="automation@solidfire.com", password="solidfire")
    steps = at2_api.list_steps_for_task_instance_id(tid)
    for step in steps:
        tastid = step.get("taskAsStepTaskInstanceID", None)
        if step["stepDisplayName"] in dnames:
            tsteps.append(step)
        elif tastid:
            rec_steps = get_steps_by_display_names(tastid, dnames)
            tsteps.extend(rec_steps)
    return tsteps


def find_step_by_display_name_exists(tid, dnames):
    at2_api = LegacyAt2Api(username="automation@solidfire.com", password="solidfire")
    steps = at2_api.list_steps_for_task_instance_id(tid)
    for step in steps:
        tastid = step.get("taskAsStepTaskInstanceID", None)
        if tastid is not None:
            exists = find_step_by_display_name_exists(tastid, dnames)
            if exists:
                return True
        elif step["stepDisplayName"] in dnames:
            return True
    return False


def find_step_by_step_name_exists(tid, snames):
    at2_api = LegacyAt2Api(username="automation@solidfire.com", password="solidfire")
    steps = at2_api.list_steps_for_task_instance_id(tid)
    for step in steps:
        tastid = step.get("taskAsStepTaskInstanceID", None)
        if tastid is not None:
            exists = find_step_by_step_name_exists(tastid, snames)
            if exists:
                return True
        else:
            if step["stepName"] in snames:
                return True
    return False


def get_tags_for_tasks(task_instance_id):
    at2_api = LegacyAt2Api(username="automation@solidfire.com", password="solidfire")
    return at2_api.get_tags_for_task(task_instance_id)


def get_bond_mode_tested_on_task(task_instance_id):
    """
    """
    bond_modes = {}
    at2_api = LegacyAt2Api(username="automation@solidfire.com", password="solidfire")
    steps = at2_api.list_steps_for_task_instance_id(task_instance_id)
    for step in steps:
        if step["stepDisplayName"] == LACP_STEP_DISPLAY_NAME:
            bond_mode = get_step_input_values(step, ["bond_mode"])
            interface = get_step_input_values(step, ["interface"])
            bond_modes[interface[0]] = bond_mode[0]
        elif step["stepDisplayName"] == CREATE_CLUSTER_WRAPPER:
            bond_modes["Bond10G"] = "LACP"
            bond_modes["Bond1G"] = "ALB"
    if "Bond10G" not in bond_modes:
        bond_modes["Bond10G"] = DEFAULT_BOND_MODE
    elif "Bond1G" not in bond_modes:
        bond_modes["Bond1G"] = DEFAULT_BOND_MODE
    return (bond_modes["Bond1G"], bond_modes["Bond10G"])


def get_network_config_info_from_tasks():
    """
    :return:
    """
    data = []
    suites = get_suites_by_epic(REGRESSION_CHUNKS_EPIC_IDS)
    for suite in suites:
        lacp_task_instances = LACP_SUTIES.get(suite['task_instance_id'], None)
        if lacp_task_instances:
            bond_mode_list = []
            for task in lacp_task_instances:
                bond_modes = get_bond_mode_tested_on_task(task)

                bond_mode_list.append(bond_modes)
        else:
            bond_mode_list = [(DEFAULT_BOND_MODE, DEFAULT_BOND_MODE)]
        for bond_mode, count in Counter(bond_mode_list).items():
            dt = {"suite_name": suite["suite_name"],
                  "epic_id": suite["epic_id"],
                  "task_id": suite["task_id"],
                  "task_instance_id": suite["task_instance_id"],
                  "suite_run_id": suite["suite_run_id"]
                  }
            if not lacp_task_instances:
                dt["testcases"] = suite['total_testcases']
            else:
                dt["testcases"] = count
            dt["Bond1G"] = bond_mode[0]
            dt["Bond10G"] = bond_mode[1]
            dt["mip_on_bond10G"] = 0
            data.append(dt)
    return data


def write_network_config_data_to_csv():
    data = get_network_config_info_from_tasks()
    file_name = "{}/network_config_info.csv".format(DATA_DIR)
    field_names = ["suite_name", "epic_id", "task_id", "task_instance_id", "suite_run_id", "testcases",
                   "Bond1G", "Bond10G", "mip_on_bond10G"]
    write_data_to_csv(data, file_name, fieldnames=field_names)


def is_tagged(task_isntance_id, tag):
    lt = get_tags_for_tasks(task_isntance_id, tag)
    if lt:
        return True
    else:
        False


def get_cluster_full_instances():
    """
    :return:
    """
    data = []
    suites = get_suites_by_epic(REGRESSION_CHUNKS_EPIC_IDS)
    for suite in suites:
        tags = get_tags_for_tasks(suite["task_instance_id"])
        cfull_stages = []
        for tag in tags:
            if tag in CFULL_TAGS_STAGES:
                cfull_stages = CFULL_TAGS_STAGES[tag]
                break
        if cfull_stages:
            for stage in cfull_stages:
                dt = {"block_cluster_full": stage,
                      "slice_cluster_full": "stage1",
                      "suite_name": suite["suite_name"],
                      "epic_id": suite["epic_id"],
                      "task_id": suite["task_id"],
                      "task_instance_id": suite["task_instance_id"],
                      "suite_run_id": suite["suite_run_id"],
                      "testcases": suite['total_testcases']
                      }
                pprint.pprint(dt)
                data.append(dt)
        else:
            tcases = 0
            testcases = get_test_cases_from_task_instance(suite["task_instance_id"])
            for testcase in testcases:
                if testcase["taskAsStepTaskInstanceID"] is None:
                    continue
                result = find_step_by_display_name_exists(testcase["taskAsStepTaskInstanceID"], FILL_TO_STAGE_N_TASKS)
                if result:
                    tcases += 1
            if tcases > 0:
                dt = {"block_cluster_full": "stage2",
                      "slice_cluster_full": "stage1",
                      "suite_name": suite["suite_name"],
                      "epic_id": suite["epic_id"],
                      "task_id": suite["task_id"],
                      "task_instance_id": suite["task_instance_id"],
                      "suite_run_id": suite["suite_run_id"],
                      "testcases": tcases
                      }
                pprint.pprint(dt)
                data.append(dt)
            elif len(testcases) - tcases > 0:
                dt = {"block_cluster_full": "stage2",
                      "slice_cluster_full": "stage1",
                      "suite_name": suite["suite_name"],
                      "epic_id": suite["epic_id"],
                      "task_id": suite["task_id"],
                      "task_instance_id": suite["task_instance_id"],
                      "suite_run_id": suite["suite_run_id"],
                      "testcases": len(testcases) - tcases
                      }
                pprint.pprint(dt)
                data.append(dt)
    return data


def write_cluster_fullto_csv():
    data = get_cluster_full_instances()
    file_name = "{}/cluster_full.csv".format(DATA_DIR)
    field_names = ["suite_name", "epic_id", "task_id", "task_instance_id", "suite_run_id", "testcases",
                   "block_cluster_full", "slice_cluster_full"]
    write_data_to_csv(data, file_name, fieldnames=field_names)


def ear_enabled_tests():
    data = []
    suites = get_suites_by_epic(REGRESSION_CHUNKS_EPIC_IDS)
    for suite in suites:
        if suite["suite_name"] in AUTO_DISABLED_EAR_SUITES:
            dt = {"suite_name": suite["suite_name"],
                  "epic_id": suite["epic_id"],
                  "task_id": suite["task_id"],
                  "task_instance_id": suite["task_instance_id"],
                  "suite_run_id": suite["suite_run_id"],
                  "testcases": suite["total_testcases"],
                  "EAR": "disabled"
                  }
            data.append(dt)
        else:
            tcases = 0
            testcases = get_test_cases_from_task_instance(suite["task_instance_id"])
            for testcase in testcases:
                if testcase["taskAsStepTaskInstanceID"] is None:
                    continue
                result = find_step_by_step_name_exists(testcase["taskAsStepTaskInstanceID"],
                                                       ENABLE_ENCRYPTION_AT_REST)
                if result:
                    tcases += 1
            if tcases > 0:
                dt = {"suite_name": suite["suite_name"],
                      "epic_id": suite["epic_id"],
                      "task_id": suite["task_id"],
                      "task_instance_id": suite["task_instance_id"],
                      "suite_run_id": suite["suite_run_id"],
                      "testcases": tcases,
                      "EAR": "enabled"
                      }
                data.append(dt)
            elif len(testcases) - tcases > 0:
                dt = {"suite_name": suite["suite_name"],
                      "epic_id": suite["epic_id"],
                      "task_id": suite["task_id"],
                      "task_instance_id": suite["task_instance_id"],
                      "suite_run_id": suite["suite_run_id"],
                      "testcases": len(testcases) - tcases,
                      "EAR": "disabled"
                      }
                data.append(dt)
    return data


def write_ear_enabled_tests():
    data = ear_enabled_tests()
    file_name = "{}/ear_enabled.csv".format(DATA_DIR)
    field_names = ["suite_name", "epic_id", "task_id", "task_instance_id", "suite_run_id", "testcases",
                   "EAR"]
    write_data_to_csv(data, file_name, fieldnames=field_names)


def get_lacp_tests():
    """
    :return:
    """
    testcase_info = {}
    for sid, tid in LACP_SUTIES.items():
        bond_mode = get_bond_mode_tested_on_task(tid)
        testcase_info[tid] = bond_mode
    return testcase_info


def get_suite_names_ids():
    dt = {}
    suites = get_suites_by_epic(REGRESSION_CHUNKS_EPIC_IDS)
    for suite in suites:
        dt[suite["suite_name"]] = suite["task_instance_id"]
    return dt


if __name__ == "__main__":
    write_suite_info_manual()